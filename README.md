# DNET Role Management

DNET Role Management is a service which is communicating with [AAI Registry
service](https://docs.google.com/document/d/1v4URGvd-mtW6Lcsk_fmV61SG98Go_KN6zsv6q4jx3IY/edit?usp=sharing) and provides methods to retrieve users information, assign roles to users or
revoke roles from them.

# Configuration

#### Registry Properties

    registry.coid=2 # OpenAIRE coid, do not change
    registry.issuer=https://openaire-dev.aai-dev.grnet.gr/registry/
    registry.user=user # Ask AAI credentials for the above issuer
    registry.password=pass # Ask AAI credentials for the above issuer
    registry.version=1.0 # Do not change

#### Redis Properties

    redis.host = localhost
    redis.port = 6379
    redis.password =
    
### Use Swagger

Project supports swagger UI documentation
(use locally spring-boot profile "swagger" in order to activate)

