package eu.dnetlib.dnetrolemanagement.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.dnetlib.dnetrolemanagement.utils.HttpUtils;
import eu.dnetlib.dnetrolemanagement.utils.JsonUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RegistryService {

    private static final Logger logger = LogManager.getLogger(RegistryService.class);

    public HttpUtils httpUtils;

    public JsonUtils jsonUtils;

    @Autowired
    public RegistryService(HttpUtils httpUtils, JsonUtils jsonUtils) {
        this.httpUtils = httpUtils;
        this.jsonUtils = jsonUtils;
    }

    /**
     * 1.1 Get CoPersonId by Email
     */
    public List<Integer> getCoPersonIdsByEmail(String email, String identifier) {
        if (email != null) {
            List<Integer> coPersonIds = new ArrayList<>();
            Map<String, String> params = new HashMap<>();
            params.put("mail", email);
            JsonElement response = httpUtils.get("co_people.json", params);
            if (response != null) {
                JsonArray coPeople = response.getAsJsonObject().get("CoPeople").getAsJsonArray();
                for (int i = 0; i < coPeople.size(); i++) {
                    coPersonIds.add(coPeople.get(i).getAsJsonObject().get("Id").getAsInt());
                }
            }
            return coPersonIds;
        } else {
            Integer coPersonId = identifier != null ? getCoPersonIdByIdentifier(identifier): getCoPersonIdByIdentifier();
            return (coPersonId != null) ? Collections.singletonList(coPersonId) : new ArrayList<>();
        }
    }

    public List<Integer> getCoPersonIdsByEmail(String email) {
        return getCoPersonIdsByEmail(email, null);
    }

    /**
     * 1.2 Get CoPersonId by AAI identifier
     */
    public Integer getCoPersonIdByIdentifier() {
        try {
            OIDCAuthenticationToken authentication = (OIDCAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
            String sub = authentication.getUserInfo().getSub();
            return getCoPersonIdByIdentifier(sub);
        } catch (Exception e) {
            logger.error("Get User info: An error occurred ", e);
            return null;
        }
    }

    public Integer getCoPersonIdByIdentifier(String sub) {
        Map<String, String> params = new HashMap<>();
        params.put("search.identifier", sub);
        JsonElement response = httpUtils.get("co_people.json", params);
        return (response != null) ? response.getAsJsonObject().get("CoPeople").getAsJsonArray().get(0).getAsJsonObject().get("Id").getAsInt() : null;
    }


    /**
     * 2 Get AAI identifier by CoPersonId
     */
    public String getIdentifierByCoPersonId(Integer coPersonId) {
        Map<String, String> params = new HashMap<>();
        params.put("copersonid", coPersonId.toString());
        JsonElement response = httpUtils.get("identifiers.json", params);
        JsonArray ids = (response != null) ? response.getAsJsonObject().get("Identifiers").getAsJsonArray() : new JsonArray();
        if (ids.size() > 0) {
            return ids.get(0).getAsJsonObject().get("Identifier").getAsString();
        }
        return null;
    }

    /**
     * 3.1 Get OpenAIRE cous with a specific name(or substring)
     */
    public JsonArray getCous(String name) {
        Map<String, String> params = new HashMap<>();
        if (name != null) {
            params.put("name", name.toLowerCase());
        }
        JsonElement response = httpUtils.get("cous.json", params);
        return (response != null) ? response.getAsJsonObject().get("Cous").getAsJsonArray() : new JsonArray();
    }

    /**
     * 3.2 Get all OpenAIRE cous
     */
    public JsonArray getCous() {
        return getCous(null);
    }

    /**
     * 4.1 Get a couId by name
     *
     * @param name
     * @return
     */
    public Integer getCouId(String name) {
        JsonArray cous = getCous(name);
        for (JsonElement cou : cous) {
            if (cou.getAsJsonObject().get("Name").getAsString().equalsIgnoreCase(name)) {
                return cou.getAsJsonObject().get("Id").getAsInt();
            }
        }
        return null;
    }

    /**
     * 5. Get User non admin roles
     */
    public JsonArray getRoles(Integer coPersonId) {
        Map<String, String> params = new HashMap<>();
        params.put("copersonid", coPersonId.toString());
        JsonElement response = httpUtils.get("co_person_roles.json", params);
        return (response != null) ? response.getAsJsonObject().get("CoPersonRoles").getAsJsonArray() : new JsonArray();
    }

    /**
     * 6. Get Role id of User base on couId.
     */
    public Integer getRoleId(Integer coPersonId, Integer couId) {
        JsonArray roles = getRoles(coPersonId);
        for (JsonElement role : roles) {
            JsonObject object = role.getAsJsonObject();
            if (object.get("CouId").getAsInt() == couId && (object.get("Status").getAsString().equals("Active") || object.get("Status").getAsString().equals("GracePeriod"))) {
                return object.get("Id").getAsInt();
            }
        }
        return null;
    }

    /**
     * 7. Get User Groups
     */
    public JsonArray getUserGroups(Integer coPersonId) {
        Map<String, String> params = new HashMap<>();
        params.put("copersonid", coPersonId.toString());
        JsonElement response = httpUtils.get("co_groups.json", params);
        return (response != null) ? response.getAsJsonObject().get("CoGroups").getAsJsonArray() : new JsonArray();
    }

    /**
     * 8. Get User Admin Group of a Cou
     */
    public JsonObject getUserAdminGroup(Integer coPersonId, Integer couId) {
        Map<String, String> params = new HashMap<>();
        params.put("copersonid", coPersonId.toString());
        JsonElement response = httpUtils.get("co_groups.json", params);
        JsonArray roles = (response != null) ? response.getAsJsonObject().get("CoGroups").getAsJsonArray() : new JsonArray();
        for (JsonElement role : roles) {
            JsonObject object = role.getAsJsonObject();
            if (object.get("CouId") != null && object.get("CouId").getAsInt() == couId) {
                if (object.get("Name").getAsString().contains("admins")) {
                    return object;
                }
            }
        }
        return null;
    }

    /**
     * 9. Get Groups of a Cou
     */
    public JsonArray getCouGroups(Integer couId) {
        Map<String, String> params = new HashMap<>();
        params.put("couid", couId.toString());
        JsonElement response = httpUtils.get("co_groups.json", params);
        return (response != null) ? response.getAsJsonObject().get("CoGroups").getAsJsonArray() : new JsonArray();
    }

    /**
     * 10. Get Admin Group of a Cou
     */
    public JsonObject getCouAdminGroup(Integer couId) {
        JsonArray groups = getCouGroups(couId);
        for (JsonElement group : groups) {
            if (group.getAsJsonObject().get("Name").getAsString().contains("admins")) {
                return group.getAsJsonObject();
            }
        }
        return null;
    }

    /**
     * 11. Get users of a group
     */
    public JsonArray getGroupMembers(Integer coGroupId) {
        Map<String, String> params = new HashMap<>();
        params.put("cogroupid", coGroupId.toString());
        JsonElement response = httpUtils.get("co_group_members.json", params);
        return (response != null) ? response.getAsJsonObject().get("CoGroupMembers").getAsJsonArray() : new JsonArray();
    }


    /**
     * 12. Get Users' email of a Cou
     */
    public JsonArray getUserEmailByCouId(Integer couId, boolean admin) {
        Map<String, String> params = new HashMap<>();
        params.put("couid", couId.toString());
        if (admin) {
            params.put("admin", "true");
        }
        JsonElement response = httpUtils.get("email_addresses.json", params);
        JsonArray infos = (response != null) ? response.getAsJsonObject().get("EmailAddresses").getAsJsonArray() : new JsonArray();
        JsonArray emails = new JsonArray();
        infos.forEach(info -> {
            JsonObject user = new JsonObject();
            user.addProperty("coPersonId", info.getAsJsonObject().get("Person").getAsJsonObject().get("Id").getAsString());
            user.addProperty("email", info.getAsJsonObject().get("Mail").getAsString());
            user.addProperty("memberSince", info.getAsJsonObject().get("Created").getAsString());
            emails.add(user);
        });
        return emails;
    }

    /**
     * 13. Get Users' names of a Cou
     */
    public JsonArray getUserNamesByCouId(Integer couId, boolean admin) {
        Map<String, String> params = new HashMap<>();
        params.put("couid", couId.toString());
        if (admin) {
            params.put("admin", "true");
        }
        JsonElement response = httpUtils.get("names.json", params);
        JsonArray infos = (response != null) ? response.getAsJsonObject().get("Names").getAsJsonArray() : new JsonArray();
        JsonArray names = new JsonArray();
        infos.forEach(info -> {
            JsonObject user = new JsonObject();
            user.addProperty("coPersonId", info.getAsJsonObject().get("Person").getAsJsonObject().get("Id").getAsString());
            user.addProperty("name", info.getAsJsonObject().get("Given").getAsString() + " " + info.getAsJsonObject().get("Family").getAsString());
            user.addProperty("memberSince", info.getAsJsonObject().get("Created").getAsString());
            names.add(user);
        });
        return names;
    }

    /**
     * 14. Get Users' identifiers of a Cou
     */
    public JsonArray getUserIdByCouId(Integer couId, boolean admin) {
        Map<String, String> params = new HashMap<>();
        params.put("couid", couId.toString());
        if (admin) {
            params.put("admin", "true");
        }
        JsonElement response = httpUtils.get("identifiers.json", params);
        JsonArray infos = (response != null) ? response.getAsJsonObject().get("Identifiers").getAsJsonArray() : new JsonArray();
        JsonArray ids = new JsonArray();
        infos.forEach(info -> {
            JsonObject user = new JsonObject();
            user.addProperty("coPersonId", info.getAsJsonObject().get("Person").getAsJsonObject().get("Id").getAsString());
            user.addProperty("id", info.getAsJsonObject().get("Identifier").getAsString());
            user.addProperty("memberSince", info.getAsJsonObject().get("Created").getAsString());
            ids.add(user);
        });
        return ids;
    }

    /**
     * 15. Assign a member role to a User
     */
    public void assignMemberRole(Integer coPersonId, Integer couId) {
        httpUtils.post("co_person_roles.json", jsonUtils.coPersonRoles(coPersonId, couId, "Active"));
    }

    /**
     * 16. Remove a member role from a User
     */
    public void removeMemberRole(Integer coPersonId, Integer couId, Integer id) {
        if (id != null) {
            httpUtils.put("co_person_roles/" + id + ".json", jsonUtils.coPersonRoles(coPersonId, couId, "Deleted"));
        }
    }

    /**
     * 17. Create a new role
     */
    public Integer createRole(String name, String description) {
        JsonElement element = httpUtils.post("cous.json", jsonUtils.createNewCou(name, description));
        return element.getAsJsonObject().get("Id").getAsInt();
    }

    /**
     * 18. Get User's email
     */
    public String getUserEmail(Integer coPersonId) {
        Map<String, String> params = new HashMap<>();
        params.put("copersonid", coPersonId.toString());
        JsonElement response = httpUtils.get("email_addresses.json", params);
        JsonObject info = (response != null) ? response.getAsJsonObject().get("EmailAddresses").getAsJsonArray().get(0).getAsJsonObject() : null;
        return (info != null) ? info.getAsJsonObject().get("Mail").getAsString() : null;
    }

    /**
     * 19. Get User's names
     */
    public String getUserNames(Integer coPersonId) {
        Map<String, String> params = new HashMap<>();
        params.put("copersonid", coPersonId.toString());
        JsonElement response = httpUtils.get("names.json", params);
        JsonObject info = (response != null) ? response.getAsJsonObject().get("Names").getAsJsonArray().get(0).getAsJsonObject() : null;
        return (info != null) ? info.getAsJsonObject().get("Given").getAsString() + " " + info.getAsJsonObject().get("Family").getAsString() : null;
    }

    /**
     * 20. Get User's identifier
     */
    public String getUserId(Integer coPersonId) {
        Map<String, String> params = new HashMap<>();
        params.put("copersonid", coPersonId.toString());
        JsonElement response = httpUtils.get("identifiers.json", params);
        JsonObject info = (response != null) ? response.getAsJsonObject().get("Identifiers").getAsJsonArray().get(0).getAsJsonObject() : null;
        return (info != null) ? info.getAsJsonObject().get("Identifier").getAsString() : null;
    }

    /**
     * 21. Assign an admin role to a User
     */
    public void assignAdminRole(Integer coPersonId, Integer couId) {
        JsonObject group = getCouAdminGroup(couId);
        if (group != null) {
            httpUtils.post("co_group_members.json", jsonUtils.coGroupMembers(group.get("Id").getAsInt(), coPersonId, true));
        }
    }

    /**
     * 22. Remove an admin role from a User
     */
    public void removeAdminRole(Integer coPersonId, Integer couId) {
        JsonObject adminGroup = this.getCouAdminGroup(couId);
        JsonArray admins = this.getGroupMembers(adminGroup.get("Id").getAsInt());
        Integer id = null;
        for (JsonElement admin : admins) {
            if (admin.getAsJsonObject().get("Person").getAsJsonObject().get("Id").getAsInt() == coPersonId) {
                id = admin.getAsJsonObject().get("Id").getAsInt();
            }
        }
        if (id != null) {
            httpUtils.delete("co_group_members/" + id + ".json");
        }
    }
}
