package eu.dnetlib.dnetrolemanagement.scripts;

import eu.dnetlib.dnetrolemanagement.controllers.CuratorController;
import eu.dnetlib.dnetrolemanagement.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Profile("monitor-curator")
public class AssignMonitorCurator implements CommandLineRunner {

    @Autowired
    CuratorController curatorController;

    @Override
    public void run(String... args) throws Exception {
        List<String> types = Arrays.asList("funder", "community", "institution", "project");
        List<User> monitorCurators = Arrays.stream(curatorController.getAll("monitor", true, false).getBody()).collect(Collectors.toList());
        types.forEach(type -> {
            User[] users = curatorController.getAll(type, true, false).getBody();
            Arrays.stream(users).forEach(user -> {
                if(monitorCurators.stream().noneMatch(curator -> curator.getEmail().equals(user.getEmail()))) {
                    curatorController.assignRole("monitor", user.getEmail(), true);
                    System.out.println("Assigned to " + user.getEmail());
                    monitorCurators.add(user);
                }
            });
        });
    }
}
