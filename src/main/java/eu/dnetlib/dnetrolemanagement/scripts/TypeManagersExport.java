package eu.dnetlib.dnetrolemanagement.scripts;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import eu.dnetlib.dnetrolemanagement.entities.User;
import eu.dnetlib.dnetrolemanagement.services.RegistryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@Profile("export-managers")
public class TypeManagersExport implements CommandLineRunner {

    private static class Manager {
        String entity;
        String email;

        public Manager(String entity, String email) {
            this.entity = entity;
            this.email = email;
        }

        public String toCSV() {
            return entity + "," + email;
        }
    }

    private static class Community {
        String id;

        public Community(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    @Autowired
    RegistryService registryService;
    private final Logger logger = LogManager.getLogger(TypeManagersExport.class);
    private final static String TYPE = "community";
    private final static String COMMUNITIES_API = "https://services.openaire.eu/openaire/community/communities";

    @Override
    public void run(String... args) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        File file = new File(TYPE + "-managers.csv");
        List<Manager> managers = new ArrayList<>();
        JsonArray cous = registryService.getCous(TYPE + ".");
        Community[] communities = new Gson().fromJson(restTemplate.getForEntity(COMMUNITIES_API, String.class).getBody(), Community[].class);
        cous.forEach(cou -> {
            String entity = cou.getAsJsonObject().get("Name").getAsString().replace(TYPE + ".", "");
            if(Arrays.stream(communities).anyMatch(community -> community.getId().equals(entity))) {
                Integer couId = cou.getAsJsonObject().get("Id").getAsInt();
                User[] emails =  new Gson().fromJson(registryService.getUserEmailByCouId(couId, true), User[].class);
                for(User email:emails) {
                    managers.add(new Manager(entity, email.getEmail()));
                }
            }
        });
        try (PrintWriter pw = new PrintWriter(file)) {
            pw.println(TYPE + ",email");
            managers.stream()
                    .map(Manager::toCSV)
                    .forEach(pw::println);
            logger.info("Export has been finished");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
