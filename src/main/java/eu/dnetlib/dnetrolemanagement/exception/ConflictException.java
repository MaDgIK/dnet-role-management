package eu.dnetlib.dnetrolemanagement.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)  // 409
public class ConflictException extends RuntimeException {

    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(String message, Throwable err) {
        super(message, err);
    }

    public HttpStatus getStatus() {
        return HttpStatus.NOT_FOUND;
    }
}

