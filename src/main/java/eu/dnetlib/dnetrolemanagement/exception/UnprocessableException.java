package eu.dnetlib.dnetrolemanagement.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)  // 422
public class UnprocessableException extends RuntimeException {

    public UnprocessableException(String message) {
        super(message);
    }

    public UnprocessableException(String message, Throwable err) {
        super(message, err);
    }

    public HttpStatus getStatus() {
        return HttpStatus.NOT_FOUND;
    }
}

