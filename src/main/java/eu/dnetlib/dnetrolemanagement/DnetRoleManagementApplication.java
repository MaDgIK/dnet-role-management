package eu.dnetlib.dnetrolemanagement;

import eu.dnetlib.dnetrolemanagement.config.properties.APIProperties;
import eu.dnetlib.dnetrolemanagement.config.properties.RedisProperties;
import eu.dnetlib.dnetrolemanagement.config.properties.RegistryProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(scanBasePackages = {"eu.dnetlib.dnetrolemanagement"})
@PropertySources({
		@PropertySource("classpath:registry.properties"),
		@PropertySource(value = "classpath:dnet-override.properties", ignoreResourceNotFound = true)
})
@EnableConfigurationProperties({RegistryProperties.class, RedisProperties.class, APIProperties.class})
public class DnetRoleManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(DnetRoleManagementApplication.class, args);
	}

}
