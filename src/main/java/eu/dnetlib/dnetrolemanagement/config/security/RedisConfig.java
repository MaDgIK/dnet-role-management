package eu.dnetlib.dnetrolemanagement.config.security;

import eu.dnetlib.dnetrolemanagement.config.properties.RedisProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;

@EnableRedisHttpSession
@Configuration
public class RedisConfig {

    private static final Logger logger = LogManager.getLogger(RedisConfig.class);
    private final RedisProperties redisProperties;

    @Autowired
    public RedisConfig(RedisProperties redisProperties) {
        this.redisProperties = redisProperties;
    }

    @Bean
    public LettuceConnectionFactory connectionFactory() {
        logger.info(String.format("Redis connection listens to %s:%s ", redisProperties.getHost(), redisProperties.getPort()));
        LettuceConnectionFactory factory = new LettuceConnectionFactory(redisProperties.getHost(), Integer.parseInt(redisProperties.getPort()));
        if (redisProperties.getPassword() != null) factory.setPassword(redisProperties.getPassword());
        return factory;
    }

    @Bean
    public HttpSessionStrategy httpSessionStrategy(){
        HeaderHttpSessionStrategy headerHttpSessionStrategy = new HeaderHttpSessionStrategy();
        headerHttpSessionStrategy.setHeaderName("Session");
        return headerHttpSessionStrategy;
    }
}