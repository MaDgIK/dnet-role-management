package eu.dnetlib.dnetrolemanagement.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import eu.dnetlib.dnetrolemanagement.entities.Response;
import eu.dnetlib.dnetrolemanagement.entities.User;
import eu.dnetlib.dnetrolemanagement.exception.ResourceNotFoundException;
import eu.dnetlib.dnetrolemanagement.exception.UnprocessableException;
import eu.dnetlib.dnetrolemanagement.services.RegistryService;
import eu.dnetlib.dnetrolemanagement.utils.AuthoritiesUpdater;
import eu.dnetlib.dnetrolemanagement.utils.AuthoritiesUtils;
import eu.dnetlib.dnetrolemanagement.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private final RegistryService registryService;
    private final AuthoritiesUpdater authoritiesUpdater;
    private final Gson gson;

    @Autowired
    public AdminController(RegistryService registryService, AuthoritiesUpdater authoritiesUpdater) {
        this.registryService = registryService;
        this.authoritiesUpdater = authoritiesUpdater;
        this.gson = new Gson();
    }

    /**
     * Get the user info of the managers of a type(Community, etc.) with id(ee, egi, etc.)
     */
    @RequestMapping(value = "/{type:.+}/{id}", method = RequestMethod.GET)
    public ResponseEntity<User[]> getAll(@PathVariable("type") String type, @PathVariable("id") String id,
                                         @RequestParam(value = "email", required = false, defaultValue = "true") boolean email,
                                         @RequestParam(value = "name", required = false, defaultValue = "true") boolean name) {
        Integer couId = registryService.getCouId(AuthoritiesUtils.memberRole(type, id));
        if (couId != null) {
            JsonArray users = registryService.getUserIdByCouId(couId, true);
            JsonArray emails = (email) ? registryService.getUserEmailByCouId(couId, true) : new JsonArray();
            JsonArray names = (name) ? registryService.getUserNamesByCouId(couId, true) : new JsonArray();
            return ResponseEntity.ok(JsonUtils.mergeUserInfo(users, emails, names, gson));
        }
        throw new ResourceNotFoundException("Role has not been found");
    }

    /**
     * Assign admin role to logged-in user or user with @email
     * If role doesn't exist or user is not a member of this group already, use force=true to create and assign both roles.
     */
    @RequestMapping(value = "/{type:.+}/{id}", method = RequestMethod.POST)
    public ResponseEntity<Response> assignRole(@PathVariable("type") String type, @PathVariable("id") String id, @RequestParam(required = false) String email,
                                               @RequestParam(required = false) String identifier,
                                               @RequestParam(value = "force", defaultValue = "false") boolean force) {
        List<Integer> coPersonIds = registryService.getCoPersonIdsByEmail(email, identifier);
        if (coPersonIds.size() > 0) {
            Integer temp = registryService.getCouId(AuthoritiesUtils.memberRole(type, id));
            if (temp != null || force) {
                Integer couId = (temp != null) ? temp : registryService.createRole(AuthoritiesUtils.memberRole(type, id), "");
                AtomicBoolean assigned = new AtomicBoolean(false);
                coPersonIds.forEach(coPersonId -> {
                    if (assignRoleToAccount(coPersonId, couId, type, id, force)) {
                        assigned.set(true);
                    }
                });
                if (assigned.get()) {
                    return ResponseEntity.ok(new Response("Role has been assigned successfully"));
                } else {
                    throw new UnprocessableException("User must be a member of this group and not already admin");
                }
            }
            throw new ResourceNotFoundException("Role has not been found");
        }
        throw new ResourceNotFoundException("User has not been found");
    }

    private boolean assignRoleToAccount(Integer coPersonId, Integer couId, String type, String id, boolean force) {
        String identifier = registryService.getIdentifierByCoPersonId(coPersonId);
        Integer role = registryService.getRoleId(coPersonId, couId);
        if (role != null || force) {
            if (role == null) {
                registryService.assignMemberRole(coPersonId, couId);
                authoritiesUpdater.addRole(identifier, new SimpleGrantedAuthority(AuthoritiesUtils.member(type, id)));
            }
            if (registryService.getUserAdminGroup(coPersonId, couId) == null) {
                registryService.assignAdminRole(coPersonId, couId);
                authoritiesUpdater.addRole(identifier, new SimpleGrantedAuthority(AuthoritiesUtils.manager(type, id)));
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * Remove admin role from logged-in user or user with @email
     */
    @RequestMapping(value = "/{type:.+}/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Response> removeRole(@PathVariable("type") String type, @PathVariable("id") String id,
                                               @RequestParam(required = false) String identifier,
                                               @RequestParam(required = false) String email) {
        List<Integer> coPersonIds = registryService.getCoPersonIdsByEmail(email, identifier);
        if (coPersonIds.size() > 0) {
            Integer couId = registryService.getCouId(AuthoritiesUtils.memberRole(type, id));
            if (couId != null) {
                coPersonIds.forEach(coPersonId -> {
                    registryService.removeAdminRole(coPersonId, couId);
                    authoritiesUpdater.removeRole(registryService.getIdentifierByCoPersonId(coPersonId), new SimpleGrantedAuthority(AuthoritiesUtils.manager(type, id)));
                });
                return ResponseEntity.ok(new Response("Role has been revoked successfully"));
            }
            throw new ResourceNotFoundException("Role has not been found");
        }
        throw new ResourceNotFoundException("User has not been found");
    }

}
