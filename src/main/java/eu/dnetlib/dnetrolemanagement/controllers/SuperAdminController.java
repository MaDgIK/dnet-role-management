package eu.dnetlib.dnetrolemanagement.controllers;

import eu.dnetlib.dnetrolemanagement.entities.Response;
import eu.dnetlib.dnetrolemanagement.entities.User;
import eu.dnetlib.dnetrolemanagement.exception.ConflictException;
import eu.dnetlib.dnetrolemanagement.exception.ResourceNotFoundException;
import eu.dnetlib.dnetrolemanagement.services.RegistryService;
import eu.dnetlib.dnetrolemanagement.utils.AuthoritiesUpdater;
import eu.dnetlib.dnetrolemanagement.utils.AuthoritiesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/super")
public class SuperAdminController {

    private final RegistryService registryService;
    private final AuthoritiesUpdater authoritiesUpdater;

    @Autowired
    public SuperAdminController(RegistryService registryService, AuthoritiesUpdater authoritiesUpdater) {
        this.registryService = registryService;
        this.authoritiesUpdater = authoritiesUpdater;
    }

    /**
     * Create a new role (only for admins)
     */
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Response> createRole(@RequestParam("name") String name, @RequestParam(value = "description", required = false) String description) {
        try {
            if (registryService.getCouId(name) == null) {
                registryService.createRole(name, description != null ? description : "");
                return ResponseEntity.ok(new Response("Role has been created successfully"));
            } else {
                throw new ConflictException("This role already exists");
            }
        } catch (HttpClientErrorException e) {
            throw new ConflictException("This role already exists");
        }
    }

    /**
     * Assign portal admin role to user with @email
     *
     */
    @RequestMapping(value = "/assign", method = RequestMethod.POST)
    public ResponseEntity<Response> assignRole(@RequestParam(required = false) String email,
                                               @RequestParam(required = false, defaultValue = "false") boolean superAdmin) {
        List<Integer> coPersonIds = registryService.getCoPersonIdsByEmail(email);
        if (coPersonIds.size() > 0) {
            Integer couId = registryService.getCouId(superAdmin?AuthoritiesUtils.superAdminRole():AuthoritiesUtils.portalAdminRole());
            if (couId != null) {
                coPersonIds.forEach(coPersonId -> {
                    String identifier = registryService.getIdentifierByCoPersonId(coPersonId);
                    registryService.assignMemberRole(coPersonId, couId);
                    authoritiesUpdater.addRole(identifier, new SimpleGrantedAuthority(AuthoritiesUtils.PORTAL_ADMIN));
                });
                return ResponseEntity.ok(new Response("Role has been assigned successfully"));
            }
            throw new ResourceNotFoundException("Role has not been found");
        }
        throw new ResourceNotFoundException("User has not been found");
    }

    /**
     * Remove portal admin role from logged in user or user with @email
     */
    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    public ResponseEntity<Response> removeRole( @RequestParam(required = false) String email,
                                                @RequestParam(required = false, defaultValue = "false") boolean superAdmin) {
        List<Integer> coPersonIds = registryService.getCoPersonIdsByEmail(email);
        if (coPersonIds.size() > 0) {
            Integer couId = registryService.getCouId(superAdmin?AuthoritiesUtils.superAdminRole():AuthoritiesUtils.portalAdminRole());
            if (couId != null) {
                coPersonIds.forEach(coPersonId -> {
                    String identifier = registryService.getIdentifierByCoPersonId(coPersonId);
                    Integer role = registryService.getRoleId(coPersonId, couId);
                    registryService.removeMemberRole(coPersonId, couId, role);
                    authoritiesUpdater.removeRole(identifier, new SimpleGrantedAuthority(AuthoritiesUtils.PORTAL_ADMIN));
                });
                return ResponseEntity.ok(new Response("Role has been revoked successfully"));
            }
            throw new ResourceNotFoundException("Role has not been found");
        }
        throw new ResourceNotFoundException("User has not been found");
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getUsersByEmail(@RequestParam String email) {
        return ResponseEntity.ok(registryService.getCoPersonIdsByEmail(email).stream().map(id -> {
            User user = new User();
            user.setEmail(registryService.getUserEmail(id));
            user.setId(registryService.getUserId(id));
            user.setName(registryService.getUserNames(id));
            return user;
        }).collect(Collectors.toList()));
    }
}
