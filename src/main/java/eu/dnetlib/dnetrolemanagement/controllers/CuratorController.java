package eu.dnetlib.dnetrolemanagement.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import eu.dnetlib.dnetrolemanagement.entities.Response;
import eu.dnetlib.dnetrolemanagement.entities.User;
import eu.dnetlib.dnetrolemanagement.exception.ConflictException;
import eu.dnetlib.dnetrolemanagement.exception.ResourceNotFoundException;
import eu.dnetlib.dnetrolemanagement.services.RegistryService;
import eu.dnetlib.dnetrolemanagement.utils.AuthoritiesUpdater;
import eu.dnetlib.dnetrolemanagement.utils.AuthoritiesUtils;
import eu.dnetlib.dnetrolemanagement.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

@RestController
@RequestMapping("/curator")
public class CuratorController {

    private final RegistryService registryService;
    private final AuthoritiesUpdater authoritiesUpdater;
    private final Gson gson;

    @Autowired
    public CuratorController(RegistryService registryService, AuthoritiesUpdater authoritiesUpdater) {
        this.registryService = registryService;
        this.authoritiesUpdater = authoritiesUpdater;
        this.gson = new Gson();
    }

    /**
     * Create a new Curator role (only for admins)
     */
    @RequestMapping(value = "/{type:.+}/create", method = RequestMethod.POST)
    public ResponseEntity<Response> createRole(@PathVariable("type") String type, @RequestParam(value = "description", required = false) String description) {
        try {
            System.out.println(type);
            if (registryService.getCouId(AuthoritiesUtils.curatorRole(type)) == null) {
                registryService.createRole(AuthoritiesUtils.curatorRole(type), description != null?description:"");
                return ResponseEntity.ok(new Response("Role has been created successfully"));
            } else {
                throw new ConflictException("This role already exists");
            }
        } catch (HttpClientErrorException e) {
            throw new ConflictException("This role already exists");
        }
    }

    /**
     * Get the user info of the curators of a type(Community, etc.)
     */
    @RequestMapping(value = "/{type:.+}", method = RequestMethod.GET)
    public ResponseEntity<User[]> getAll(@PathVariable("type") String type,
                                         @RequestParam(value = "email", required = false, defaultValue = "true") boolean email,
                                         @RequestParam(value = "name", required = false, defaultValue = "true") boolean name) {
        Integer couId = registryService.getCouId(AuthoritiesUtils.curatorRole(type));
        if (couId != null) {
            JsonArray users = registryService.getUserIdByCouId(couId, false);
            JsonArray emails = (email) ? registryService.getUserEmailByCouId(couId, false) : new JsonArray();
            JsonArray names = (name) ? registryService.getUserNamesByCouId(couId, false) : new JsonArray();
            return ResponseEntity.ok(JsonUtils.mergeUserInfo(users, emails, names, gson));
        }
        throw new ResourceNotFoundException("Role has not been found");
    }

    /**
     * Assign curator role to logged in user or user with @email
     */
    @RequestMapping(value = "/{type:.+}", method = RequestMethod.POST)
    public ResponseEntity<Response> assignRole(@PathVariable("type") String type, @RequestParam(required = false) String email,
                                               @RequestParam(value = "force", defaultValue = "false") boolean force) {
        List<Integer> coPersonIds = registryService.getCoPersonIdsByEmail(email);
        if (coPersonIds.size() > 0) {
            Integer temp = registryService.getCouId(AuthoritiesUtils.curatorRole(type));
            if (temp != null || force) {
                Integer couId = (temp != null)?temp:registryService.createRole(AuthoritiesUtils.curatorRole(type), "");
                coPersonIds.forEach(coPersonId -> {
                    String identifier = registryService.getIdentifierByCoPersonId(coPersonId);
                    registryService.assignMemberRole(coPersonId, couId);
                    authoritiesUpdater.addRole(identifier, new SimpleGrantedAuthority(AuthoritiesUtils.curator(type)));
                });
                return ResponseEntity.ok(new Response("Role has been assigned successfully"));
            }
            throw new ResourceNotFoundException("Role has not been found");
        }
        throw new ResourceNotFoundException("User has not been found");
    }

    /**
     * Remove curator role from logged in user or user with @email
     */
    @RequestMapping(value = "/{type:.+}", method = RequestMethod.DELETE)
    public ResponseEntity<Response> removeRole(@PathVariable("type") String type, @RequestParam(required = false) String email) {
        List<Integer> coPersonIds = registryService.getCoPersonIdsByEmail(email);
        if (coPersonIds.size() > 0) {
            Integer couId = registryService.getCouId(AuthoritiesUtils.curatorRole(type));
            if (couId != null) {
                coPersonIds.forEach(coPersonId -> {
                    String identifier = registryService.getIdentifierByCoPersonId(coPersonId);
                    Integer role = registryService.getRoleId(coPersonId, couId);
                    registryService.removeMemberRole(coPersonId, couId, role);
                    authoritiesUpdater.removeRole(identifier, new SimpleGrantedAuthority(AuthoritiesUtils.curator(type)));
                });
                return ResponseEntity.ok(new Response("Role has been revoked successfully"));
            }
            throw new ResourceNotFoundException("Role has not been found");
        }
        throw new ResourceNotFoundException("User has not been found");
    }
}
