package eu.dnetlib.dnetrolemanagement.controllers;

import eu.dnetlib.dnetrolemanagement.config.properties.APIProperties;
import eu.dnetlib.dnetrolemanagement.config.properties.RedisProperties;
import eu.dnetlib.dnetrolemanagement.config.properties.RegistryProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HealthController {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final APIProperties apiProperties;
    private final RedisProperties redisProperties;
    private final RegistryProperties registryProperties;

    @Autowired
    public HealthController(APIProperties apiProperties, RedisProperties redisProperties, RegistryProperties registryProperties) {
        this.apiProperties = apiProperties;
        this.redisProperties = redisProperties;
        this.registryProperties = registryProperties;
    }

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from role management!");
        return "Hello from Role management!";
    }

    @PreAuthorize("hasAnyAuthority('PORTAL_ADMINISTRATOR')")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();
        response.put("api.title", apiProperties.getTitle());
        response.put("api.description", apiProperties.getDescription());
        response.put("api.version", apiProperties.getVersion());
        response.put("registry.coid", registryProperties.getCoid());
        response.put("registry.issuer", registryProperties.getIssuer());
        response.put("registry.user", registryProperties.getUser());
        response.put("registry.password", registryProperties.getPassword());
        response.put("registry.version", registryProperties.getVersion());
        response.put("redis.host", redisProperties.getHost());
        return response;
    }
}
