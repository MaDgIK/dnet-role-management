package eu.dnetlib.dnetrolemanagement.utils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import eu.dnetlib.dnetrolemanagement.config.properties.RegistryProperties;
import eu.dnetlib.dnetrolemanagement.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

@Component
public class JsonUtils {

    private final String version;
    private final String coid;

    @Autowired
    public JsonUtils(RegistryProperties registryProperties) {
        this.version = registryProperties.getVersion();
        this.coid = registryProperties.getCoid();
    }

    public static User[] mergeUserInfo(JsonArray users, JsonArray emails, JsonArray names, Gson gson) {
        User[] usersMapped =  gson.fromJson(users, User[].class);
        User[] emailsMapped =  gson.fromJson(emails, User[].class);
        User[] namesMapped =  gson.fromJson(names, User[].class);
        for(User user: usersMapped) {
            Optional<User> emailUser = Arrays.stream(emailsMapped).filter(email -> user.getCoPersonId().equals(email.getCoPersonId())).findFirst();
            Optional<User> nameUser = Arrays.stream(namesMapped).filter(name -> user.getCoPersonId().equals(name.getCoPersonId())).findFirst();
            emailUser.ifPresent(value -> user.setEmail(value.getEmail()));
            nameUser.ifPresent(value -> user.setName(value.getName()));
        }
        return usersMapped;
    }

    public JsonObject coPersonRoles(Integer coPersonId, Integer couId, String status) {
        JsonObject role = new JsonObject();
        JsonArray coPersonRoles = new JsonArray();
        JsonObject coPersonRole = new JsonObject();
        JsonObject person = new JsonObject();
        person.addProperty("Type", "CO");
        person.addProperty("Id", coPersonId.toString());
        coPersonRole.addProperty("Version", version);
        coPersonRole.add("Person", person);
        coPersonRole.addProperty("CouId", couId.toString());
        coPersonRole.addProperty("Affiliation", "member");
        coPersonRole.addProperty("Title", "");
        coPersonRole.addProperty("O", "Openaire");
        coPersonRole.addProperty("Status", status);
        if(status.equals("Active")) {
            coPersonRole.addProperty("ValidFrom", new Date().toString());
        } else {
            coPersonRole.addProperty("ValidThrough", new Date().toString());
        }
        coPersonRoles.add(coPersonRole);
        role.addProperty("RequestType", "CoPersonRoles");
        role.addProperty("Version", version);
        role.add("CoPersonRoles", coPersonRoles);
        return role;
    }

    public JsonObject coGroupMembers(Integer coGroupId, Integer coPersonId, boolean member) {
        JsonObject coGroup = new JsonObject();
        JsonArray coGroupMembers = new JsonArray();
        JsonObject coGroupMember = new JsonObject();
        JsonObject person = new JsonObject();
        person.addProperty("Type", "CO");
        person.addProperty("Id", coPersonId.toString());
        coGroupMember.addProperty("Version", version);
        coGroupMember.add("Person", person);
        coGroupMember.addProperty("CoGroupId", coGroupId.toString());
        coGroupMember.addProperty("Member", member);
        coGroupMember.addProperty("Owner", false);
        coGroupMember.addProperty("ValidFrom", "");
        coGroupMember.addProperty("ValidThrough", "");
        coGroupMembers.add(coGroupMember);
        coGroup.addProperty("RequestType", "CoGroupMembers");
        coGroup.addProperty("Version", version);
        coGroup.add("CoGroupMembers", coGroupMembers);
        return coGroup;
    }

    public JsonObject createNewCou(String name, String description) {
        JsonObject cou = new JsonObject();
        JsonArray cous = new JsonArray();
        JsonObject newCou = new JsonObject();
        newCou.addProperty("Version", version);
        newCou.addProperty("CoId", coid);
        newCou.addProperty("Name",name);
        newCou.addProperty("Description", description);
        cous.add(newCou);
        cou.addProperty("RequestType", "Cous");
        cou.addProperty("Version", version);
        cou.add("Cous", cous);
        return cou;
    }

    public JsonObject createResponse(JsonElement response) {
        JsonObject json = new JsonObject();
        json.add("response", response);
        return json;
    }

    public JsonObject createResponse(String response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public JsonObject createResponse(Number response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public JsonObject createResponse(Boolean response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }

    public JsonObject createResponse(Character response) {
        JsonObject json = new JsonObject();
        json.addProperty("response", response);
        return json;
    }
}
