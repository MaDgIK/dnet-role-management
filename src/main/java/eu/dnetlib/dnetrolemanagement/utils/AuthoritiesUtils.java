package eu.dnetlib.dnetrolemanagement.utils;

import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthoritiesUtils {

    public static String PORTAL_ADMIN = "PORTAL_ADMINISTRATOR";
    public static String SUPER_ADMIN = "SUPER_ADMINISTRATOR";

    public static String superAdminRole() {
        return "Super Administrator";
    }

    public static String portalAdminRole() {
        return "Portal Administrator";
    }

    /**
     * Type = FUNDER | COMMUNITY | INSTITUTION | PROJECT etc.
     */
    public static String curator(String type) {
        return "CURATOR_" + type.replaceAll("[.]", "_").toUpperCase();
    }

    public static String curatorRole(String type) {
        return "Curator - " + Character.toString(type.replaceAll("[.]", "_").charAt(0)).toUpperCase() + type.substring(1);
    }

    /**
     * Type = FUNDER | COMMUNITY | INSTITUTION | PROJECT etc.
     * <p>
     * Id = EE, EGI, etc
     */
    public static String manager(String type, String id) {
        return type.replaceAll("[.]", "_").toUpperCase() + "_" + id.toUpperCase() + "_MANAGER";
    }

    /**
     * Type = FUNDER | COMMUNITY | RI | INSTITUTION | PROJECT etc.
     * <p>
     * Id = EE, EGI, etc
     */
    public static String member(String type, String id) {
        return type.replaceAll("[.]", "_").toUpperCase() + "_" + id.toUpperCase();
    }

    public static String memberRole(String type, String id) {
        return type + "." + id;
    }

    public static String getAaiId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication instanceof OIDCAuthenticationToken ? ((OIDCAuthenticationToken) authentication).getSub() : null;
    }
}
