package eu.dnetlib.dnetrolemanagement.utils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import eu.dnetlib.dnetrolemanagement.config.properties.RegistryProperties;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Component
public class HttpUtils {

    private final RegistryProperties registryProperties;

    @Autowired
    public HttpUtils(RegistryProperties registryProperties) {
        this.registryProperties = registryProperties;
    }

    public JsonElement post(String path, JsonObject body) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = createHeaders(registryProperties.getUser(), registryProperties.getPassword());
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(registryProperties.getIssuer() + path, HttpMethod.POST, request, String.class);
        if (responseEntity.getStatusCode().is2xxSuccessful() && responseEntity.getBody() != null) {
            return new JsonParser().parse(responseEntity.getBody());
        } else {
            return null;
        }
    }

    public JsonElement put(String path, JsonObject body) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = createHeaders(registryProperties.getUser(), registryProperties.getPassword());
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(body.toString(), headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(registryProperties.getIssuer() + path, HttpMethod.PUT, request, String.class);
        if (responseEntity.getBody() != null) {
            return new JsonParser().parse(responseEntity.getBody());
        } else {
            return null;
        }
    }

    public JsonElement get(String path, Map<String, String> params) {
        RestTemplate restTemplate = new RestTemplate();
        String url = registryProperties.getIssuer() + path + createParams(addCoId(params));
        ResponseEntity<String> responseEntity = restTemplate.exchange
                (url, HttpMethod.GET, new HttpEntity<>(createHeaders(registryProperties.getUser(), registryProperties.getPassword())), String.class);
        if (responseEntity.getBody() != null) {
            return new JsonParser().parse(responseEntity.getBody());
        } else {
            return null;
        }
    }

    public JsonElement delete(String path) {
        RestTemplate restTemplate = new RestTemplate();
        String url = registryProperties.getIssuer() + path;
        ResponseEntity<String> responseEntity = restTemplate.exchange
                (url, HttpMethod.DELETE, new HttpEntity<>(createHeaders(registryProperties.getUser(), registryProperties.getPassword())), String.class);
        if (responseEntity.getBody() != null) {
            return new JsonParser().parse(responseEntity.getBody());
        } else {
            return null;
        }
    }

    private Map<String, String> addCoId(Map<String, String> params) {
        if(params == null) {
            params = new HashMap<>();
        }
        params.put("coid", registryProperties.getCoid());
        return params;
    }

    private String createParams(Map<String, String> params) {
        StringBuilder ret = new StringBuilder("?");
        int count = 0;
        for (Map.Entry<String, String> param : params.entrySet()) {
            ret.append(param.getKey()).append("=");
            ret.append(param.getValue());
            count++;
            if (count != params.entrySet().size()) {
                ret.append("&");
            }
        }
        return ret.toString();
    }

    private HttpHeaders createHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }
}
