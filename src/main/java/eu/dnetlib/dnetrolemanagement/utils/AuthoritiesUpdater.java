package eu.dnetlib.dnetrolemanagement.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mitre.openid.connect.model.OIDCAuthenticationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedClientException;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.session.ExpiringSession;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;


@Service
public class AuthoritiesUpdater extends HttpSessionSecurityContextRepository {

    private final Logger logger = LogManager.getLogger(AuthoritiesUpdater.class);

    @Autowired
    FindByIndexNameSessionRepository sessions;

    public void update(String id, Update update) {
        if (sessions != null) {
            Map<String, ExpiringSession> map = sessions.
                    findByIndexNameAndIndexValue(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME, id);
            if (map != null) {
                for (ExpiringSession session : map.values()) {
                    if (!session.isExpired()) {
                        SecurityContext securityContext = session.getAttribute(SPRING_SECURITY_CONTEXT_KEY);
                        Authentication authentication = securityContext.getAuthentication();
                        if (authentication instanceof OIDCAuthenticationToken) {
                            OIDCAuthenticationToken authOIDC = (OIDCAuthenticationToken) authentication;
                            securityContext.setAuthentication(new OIDCAuthenticationToken(authOIDC.getSub(), authOIDC.getIssuer(),
                                    authOIDC.getUserInfo(), update.authorities(authOIDC.getAuthorities()), authOIDC.getIdToken(),
                                    authOIDC.getAccessTokenValue(), authOIDC.getRefreshTokenValue()));
                            session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, securityContext);
                            sessions.save(session);
                        }
                    }
                }
            }
        }
    }

    public void addRole(String id, GrantedAuthority role) {
        this.update(id, old -> {
            HashSet<GrantedAuthority> authorities = new HashSet<>(old);
            authorities.add(role);
            return authorities;
        });
    }

    public void addRole(GrantedAuthority role) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof OIDCAuthenticationToken) {
            OIDCAuthenticationToken oidcAuth = (OIDCAuthenticationToken) auth;
            this.addRole(oidcAuth.getUserInfo().getEmail(), role);
        } else {
            throw new UnauthorizedClientException("User auth is not instance of OIDCAuthenticationToken");
        }
    }

    public void removeRole(String id, GrantedAuthority role) {
        this.update(id, old -> {
            HashSet<GrantedAuthority> authorities = new HashSet<>(old);
            authorities.remove(role);
            return authorities;
        });
    }

    public void removeRole(GrantedAuthority role) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof OIDCAuthenticationToken) {
            OIDCAuthenticationToken oidcAuth = (OIDCAuthenticationToken) auth;
            this.removeRole(oidcAuth.getUserInfo().getEmail(), role);
        }
    }

    public interface Update {
        Collection<? extends GrantedAuthority> authorities(Collection<? extends GrantedAuthority> old);
    }
}
